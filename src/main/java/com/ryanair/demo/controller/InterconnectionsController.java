package com.ryanair.demo.controller;

import com.ryanair.demo.model.InterconnectionsApiModel;
import com.ryanair.demo.model.RoutesApiModel;
import com.ryanair.demo.service.IRoutesService;
import com.ryanair.demo.service.IScheduleService;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.ExecutionException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("v1")
@Slf4j
public class InterconnectionsController {

  private IRoutesService routesService;
  private IScheduleService scheduleService;

  @Autowired
  public InterconnectionsController(IRoutesService routesService,
      IScheduleService scheduleService) {
    this.routesService = routesService;
    this.scheduleService = scheduleService;
  }

  @RequestMapping("interconnections")
  public ResponseEntity<List<InterconnectionsApiModel>> getInterconnections(
      @RequestParam String departure, @RequestParam String arrival,
      @RequestParam String departureDateTime, @RequestParam String arrivalDateTime)
      throws ExecutionException, InterruptedException {

    LocalDateTime departureDateTimeFormatted = LocalDateTime
        .from(DateTimeFormatter.ISO_LOCAL_DATE_TIME.parse(departureDateTime));
    LocalDateTime arrivalDateTimeFormatted = LocalDateTime
        .from(DateTimeFormatter.ISO_LOCAL_DATE_TIME.parse(arrivalDateTime));

    log.info("departure date {}", departureDateTime);
    log.info("arrival date {}", arrivalDateTime);

    List<RoutesApiModel> routesList = routesService.getRoutesList().get();

    return null;

  }

}
