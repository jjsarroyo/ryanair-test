package com.ryanair.demo.model;

import lombok.Data;

@Data
public class RoutesApiModel {

  private String airportFrom;

  private String airportTo;

  private String connectingAirport;

  private Boolean newRoute;

  private Boolean seasonalRute;

  private String operator;

  private String group;

}
