package com.ryanair.demo.model;

import lombok.Data;

@Data
public class ScheduleDays {

  private int flight;

  private ScheduleFligths flights;

}
