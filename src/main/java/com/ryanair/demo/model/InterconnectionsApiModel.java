package com.ryanair.demo.model;

import lombok.Data;

@Data
public class InterconnectionsApiModel {

  private String departure;

  private String departureDateTime;

  private String arrival;

  private String arrivalDateTime;

}
