package com.ryanair.demo.model;

import java.util.List;
import lombok.Data;

@Data
public class ScheduleApiModel {

  private int month;

  private List<ScheduleDays> days;

}
