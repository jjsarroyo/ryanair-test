package com.ryanair.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class ScheduleFligths {

  @JsonIgnore
  private String carrierCode;

  private int number;

  private String departureTime;

  private String arrivalTime;
}
