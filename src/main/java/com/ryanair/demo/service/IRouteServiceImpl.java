package com.ryanair.demo.service;

import com.ryanair.demo.model.RoutesApiModel;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Async
@Service
public class IRouteServiceImpl implements IRoutesService {

  private RestTemplate restTemplate;

  @Autowired
  public IRouteServiceImpl(RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }

  @Override
  public CompletableFuture<List<RoutesApiModel>> getRoutesList() {

    ResponseEntity<List<RoutesApiModel>> routes = restTemplate.exchange(
        "https://services-api.ryanair.com/locate/3/routes",
        HttpMethod.GET,
        null,
        new ParameterizedTypeReference<List<RoutesApiModel>>(){});
    return CompletableFuture.completedFuture(routes.getBody());
  }
}
