package com.ryanair.demo.service;

import com.ryanair.demo.model.RoutesApiModel;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public interface IRoutesService {

  CompletableFuture<List<RoutesApiModel>> getRoutesList();
}
