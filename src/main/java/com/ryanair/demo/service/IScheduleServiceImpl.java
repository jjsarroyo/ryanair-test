package com.ryanair.demo.service;

import com.ryanair.demo.model.ScheduleApiModel;
import java.time.LocalDateTime;
import java.util.concurrent.CompletableFuture;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Async
@Service
public class IScheduleServiceImpl implements IScheduleService {

  @Override
  public CompletableFuture<ScheduleApiModel> getSchedule(String departure, String arrival,
      LocalDateTime departureDataTime, LocalDateTime arrivalDateTime) {
    return null;
  }
}
