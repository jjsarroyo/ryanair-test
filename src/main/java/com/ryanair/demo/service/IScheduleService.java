package com.ryanair.demo.service;

import com.ryanair.demo.model.ScheduleApiModel;
import java.time.LocalDateTime;
import java.util.concurrent.CompletableFuture;

public interface IScheduleService {

  CompletableFuture<ScheduleApiModel> getSchedule(String departure, String arrival,
      LocalDateTime departureDataTime, LocalDateTime
      arrivalDateTime);
}
